module.exports = function(grunt) {

  var mozjpeg = require('imagemin-mozjpeg');

  grunt.initConfig({
    /** Combine css files and place them in the public folder. **/
    sass: {                              // Task 
      dist: {                            // Target 
        options: {                       // Target options 
          style: 'expanded',
          require: 'susy'
        },
        files: {                                                                  // Dictionary of files
          '../public/css/main.php': 'scss/import.scss',                           // 'destination': 'source'
          '../public/css/portfolio.php': 'scss/import-portfolio.scss',
          '../public/css/slider.php': 'scss/import-slider.scss'
        }
      }
    },
    /** Uglify css files **/
    cssmin: {
      target: {
        files: {
          '../public/css/main-min.php': '../public/css/main.php',                  // 'destination': 'source'
          '../public/css/portfolio-min.php': '../public/css/portfolio.php',
          '../public/css/slider-min.php': '../public/css/slider.php'
        }
      }
    },
    /** Combine js files and place them in the public folder. **/
    concat: {
      all: {
        files: {
          '../public/js/main.js': grunt.file.readJSON('js/main-map.js'),
          '../public/js/slider.js': grunt.file.readJSON('js/slider-map.js'),
          '../public/js/portfolio.js': grunt.file.readJSON('js/portfolio-map.js')
        }
      }
    },
    /** Uglify js files **/
    uglify: {
      all: {
        files: {
          '../public/js/main-min.js': '../public/js/main.js',
          '../public/js/slider-min.js': '../public/js/slider.js',
          '../public/js/portfolio-min.js': '../public/js/portfolio.js'
        }
      }
    },
    watch: {
      js: {
        files: ['js/**/*.js'],
        tasks: ['newer:jshint:all', 'concat:all', 'notify:js']
      }
    },
    // Compress gzip assets 1-to-1 for production
    compress: {
      main: {
        options: {
          mode: 'gzip'
        },
        expand: true,
        cwd: '../public/js',
        src: ['*'],
        dest: '../public/js',
        ext: '.gz.js'
      }
    },
    responsive_images: {                  // Task 
      images: {
        options: {
          sizes: [{
            height: 15,
            quality: 60
          },{
            height: 60,
            quality: 60
          },{
            height: 120,
            quality: 60
          },{
            height: 76,
            quality: 60
          },{
            height: 152,
            quality: 60
          },{
            height: 144,
            quality: 60
          },{
            height: 288,
            quality: 60
          },{
            height: 224,
            quality: 60
          },{
            height: 448,
            quality: 60
          },{
            height: 480,
            quality: 60
          },{
            height: 960,
            quality: 60
          },{
            height: 570,
            quality: 60
          },{
            height: 1140,
            quality: 60
          }]
        },
        files: [{
          expand: true,
          src: ['**.{jpg,gif,png}'],
          cwd: 'img/src/',
          custom_dest: 'img/h{%= height %}'
        }]
      }
    },
    imagemin: {
      dynamic: {
        options: {                       // Target options
          optimizationLevel: 5,
          svgoPlugins: [{ removeViewBox: false }]
        }, // Another target
        files: [{
          expand: true,                  // Enable dynamic expansion
          cwd: 'img/',                   // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
          dest: '../public/img/'         // Destination path prefix
        }]
      }
    }
  });

  grunt.loadNpmTasks('grunt-cssc');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-responsive-images');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compress');

  grunt.registerTask('compile', ['sass','cssmin','concat','uglify']);

  grunt.registerTask('resize', ['responsive_images']);

  grunt.registerTask('optimize', ['imagemin']);

};