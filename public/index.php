<?php

include 'html/components/debug.php';

require 'application/routing/AltoRouter.php';
/*ini_set('display_errors', 1);
error_reporting(E_ALL);*/
$router = new AltoRouter();

// Define custom matchType for 2 character language code
$router->addMatchTypes(array('lang' => '[a-z]{2}'));

// Page mapping

$pages_json_url = "data/pages.json";
$pages_json = file_get_contents($pages_json_url);
$pages_json = str_replace('},]',"}]",$pages_json);
$pages = json_decode($pages_json, true);

$currentPage = $pages[0];
$languageId = 0;

foreach ($pages as $keyPages => $mapPages) {
    $require = '/html/pages/'.$mapPages['require'];

    // Map nl
    $routeNL = '/[nl:language]/'.$mapPages['language'][0]['pattern'].((strlen($mapPages['language'][0]['pattern']) > 0) ? '/': '');
    $router->map( 'GET', $routeNL, function($language, $id) use ($mapPages, $currentPage, $languageId) {
        $languageId = 0;
        $currentPage = $mapPages;
        require __DIR__ . '/html/pages/'.$mapPages['require'].'';
    });

    // Map en
    $routeEN = '/[en:language]/'.$mapPages['language'][1]['pattern'].((strlen($mapPages['language'][1]['pattern']) > 0) ? '/': '');
    $router->map( 'GET', $routeEN, function($language, $id) use ($mapPages, $currentPage, $languageId) {
        $languageId = 1;
        $currentPage = $mapPages;
        require __DIR__ . '/html/pages/'.$mapPages['require'].'';
    });

    // Map nl
    $routeNL = '/amp/[nl:language]/'.$mapPages['language'][0]['pattern'].((strlen($mapPages['language'][0]['pattern']) > 0) ? '/': '');
    $router->map( 'GET', $routeNL, function($language, $id) use ($mapPages, $currentPage, $languageId) {
        $languageId = 0;
        $currentPage = $mapPages;
        require __DIR__ . '/html_amp/pages/'.$mapPages['require'].'';
    });

    // Map en
    $routeEN = '/amp/[en:language]/'.$mapPages['language'][1]['pattern'].((strlen($mapPages['language'][1]['pattern']) > 0) ? '/': '');
    $router->map( 'GET', $routeEN, function($language, $id) use ($mapPages, $currentPage, $languageId) {
        $languageId = 1;
        $currentPage = $mapPages;
        require __DIR__ . '/html_amp/pages/'.$mapPages['require'].'';
    });
}

// Map react
$router->map( 'GET', '/react/', function($language) {
    require __DIR__ . '/application/react/react.html';
});

// Map angular
$router->map( 'GET', '/angular/', function($language) {
    require __DIR__ . '/application/angular/angular.html';
});

// Map vue
$router->map( 'GET', '/vue/', function($language) {
    require __DIR__ . '/application/vue/vue.html';
});

// Map node
$router->map( 'GET', '/node/', function($language) {
    require __DIR__ . '/application/node/node.html';
});

// Match current request url
$match = $router->match();

// Call closure or throw 404 status
if( $match && is_callable( $match['target'] ) ) {
    call_user_func_array( $match['target'], $match['params'] ); 
} else {
    // no route was matched
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    require __DIR__ . '/404.html';
}

?>