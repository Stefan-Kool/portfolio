html {
  height: 100%;
  min-height: 100%;
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

body {
  position: relative;
  min-height: 100%;
  margin: 0;
}

.clear:before, .clear:after {
  content: " ";
  display: table;
}
.clear:after {
  clear: both;
}

body {
  overflow-x: hidden;
}

.container {
  max-width: 1456px;
  margin-left: auto;
  margin-right: auto;
}
.container:after {
  content: " ";
  display: block;
  clear: both;
}

/*! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css */
/**
 * 1. Set default font family to sans-serif.
 * 2. Prevent iOS and IE text size adjust after device orientation change,
 *    without disabling user zoom.
 */
html {
  font-family: sans-serif;
  /* 1 */
  -ms-text-size-adjust: 100%;
  /* 2 */
  -webkit-text-size-adjust: 100%;
  /* 2 */
}

/**
 * Remove default margin.
 */
body {
  margin: 0;
}

/* HTML5 display definitions
   ========================================================================== */
/**
 * Correct `block` display not defined for any HTML5 element in IE 8/9.
 * Correct `block` display not defined for `details` or `summary` in IE 10/11
 * and Firefox.
 * Correct `block` display not defined for `main` in IE 11.
 */
article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
main,
menu,
nav,
section,
summary {
  display: block;
}

/**
 * 1. Correct `inline-block` display not defined in IE 8/9.
 * 2. Normalize vertical alignment of `progress` in Chrome, Firefox, and Opera.
 */
audio,
canvas,
progress,
video {
  display: inline-block;
  /* 1 */
  vertical-align: baseline;
  /* 2 */
}

/**
 * Prevent modern browsers from displaying `audio` without controls.
 * Remove excess height in iOS 5 devices.
 */
audio:not([controls]) {
  display: none;
  height: 0;
}

/**
 * Address `[hidden]` styling not present in IE 8/9/10.
 * Hide the `template` element in IE 8/9/10/11, Safari, and Firefox < 22.
 */
[hidden],
template {
  display: none;
}

/* Links
   ========================================================================== */
/**
 * Remove the gray background color from active links in IE 10.
 */
a {
  background-color: transparent;
}

/**
 * Improve readability of focused elements when they are also in an
 * active/hover state.
 */
a:active,
a:hover {
  outline: 0;
}

/* Text-level semantics
   ========================================================================== */
/**
 * Address styling not present in IE 8/9/10/11, Safari, and Chrome.
 */
abbr[title] {
  border-bottom: 1px dotted;
}

/**
 * Address style set to `bolder` in Firefox 4+, Safari, and Chrome.
 */
b,
strong {
  font-weight: bold;
}

/**
 * Address styling not present in Safari and Chrome.
 */
dfn {
  font-style: italic;
}

/**
 * Address variable `h1` font-size and margin within `section` and `article`
 * contexts in Firefox 4+, Safari, and Chrome.
 */
h1 {
  font-size: 2em;
  margin: 0.67em 0;
}

/**
 * Address styling not present in IE 8/9.
 */
mark {
  background: #ff0;
  color: #000;
}

/**
 * Address inconsistent and variable font size in all browsers.
 */
small {
  font-size: 80%;
}

/**
 * Prevent `sub` and `sup` affecting `line-height` in all browsers.
 */
sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sup {
  top: -0.5em;
}

sub {
  bottom: -0.25em;
}

/* Embedded content
   ========================================================================== */
/**
 * Remove border when inside `a` element in IE 8/9/10.
 */
img {
  border: 0;
}

/**
 * Correct overflow not hidden in IE 9/10/11.
 */
svg:not(:root) {
  overflow: hidden;
}

/* Grouping content
   ========================================================================== */
/**
 * Address margin not present in IE 8/9 and Safari.
 */
figure {
  margin: 1em 40px;
}

/**
 * Address differences between Firefox and other browsers.
 */
hr {
  box-sizing: content-box;
  height: 0;
}

/**
 * Contain overflow in all browsers.
 */
pre {
  overflow: auto;
}

/**
 * Address odd `em`-unit font size rendering in all browsers.
 */
code,
kbd,
pre,
samp {
  font-family: monospace, monospace;
  font-size: 1em;
}

/* Forms
   ========================================================================== */
/**
 * Known limitation: by default, Chrome and Safari on OS X allow very limited
 * styling of `select`, unless a `border` property is set.
 */
/**
 * 1. Correct color not being inherited.
 *    Known issue: affects color of disabled elements.
 * 2. Correct font properties not being inherited.
 * 3. Address margins set differently in Firefox 4+, Safari, and Chrome.
 */
button,
input,
optgroup,
select,
textarea {
  color: inherit;
  /* 1 */
  font: inherit;
  /* 2 */
  margin: 0;
  /* 3 */
}

/**
 * Address `overflow` set to `hidden` in IE 8/9/10/11.
 */
button {
  overflow: visible;
}

/**
 * Address inconsistent `text-transform` inheritance for `button` and `select`.
 * All other form control elements do not inherit `text-transform` values.
 * Correct `button` style inheritance in Firefox, IE 8/9/10/11, and Opera.
 * Correct `select` style inheritance in Firefox.
 */
button,
select {
  text-transform: none;
}

/**
 * 1. Avoid the WebKit bug in Android 4.0.* where (2) destroys native `audio`
 *    and `video` controls.
 * 2. Correct inability to style clickable `input` types in iOS.
 * 3. Improve usability and consistency of cursor style between image-type
 *    `input` and others.
 */
button,
html input[type="button"],
input[type="reset"],
input[type="submit"] {
  -webkit-appearance: button;
  /* 2 */
  cursor: pointer;
  /* 3 */
}

/**
 * Re-set default cursor for disabled elements.
 */
button[disabled],
html input[disabled] {
  cursor: default;
}

/**
 * Remove inner padding and border in Firefox 4+.
 */
button::-moz-focus-inner,
input::-moz-focus-inner {
  border: 0;
  padding: 0;
}

/**
 * Address Firefox 4+ setting `line-height` on `input` using `!important` in
 * the UA stylesheet.
 */
input {
  line-height: normal;
}

/**
 * It's recommended that you don't attempt to style these elements.
 * Firefox's implementation doesn't respect box-sizing, padding, or width.
 *
 * 1. Address box sizing set to `content-box` in IE 8/9/10.
 * 2. Remove excess padding in IE 8/9/10.
 */
input[type="checkbox"],
input[type="radio"] {
  box-sizing: border-box;
  /* 1 */
  padding: 0;
  /* 2 */
}

/**
 * Fix the cursor style for Chrome's increment/decrement buttons. For certain
 * `font-size` values of the `input`, it causes the cursor style of the
 * decrement button to change from `default` to `text`.
 */
input[type="number"]::-webkit-inner-spin-button,
input[type="number"]::-webkit-outer-spin-button {
  height: auto;
}

/**
 * 1. Address `appearance` set to `searchfield` in Safari and Chrome.
 * 2. Address `box-sizing` set to `border-box` in Safari and Chrome.
 */
input[type="search"] {
  -webkit-appearance: textfield;
  /* 1 */
  box-sizing: content-box;
  /* 2 */
}

/**
 * Remove inner padding and search cancel button in Safari and Chrome on OS X.
 * Safari (but not Chrome) clips the cancel button when the search input has
 * padding (and `textfield` appearance).
 */
input[type="search"]::-webkit-search-cancel-button,
input[type="search"]::-webkit-search-decoration {
  -webkit-appearance: none;
}

/**
 * Define consistent border, margin, and padding.
 */
fieldset {
  border: 1px solid #c0c0c0;
  margin: 0 2px;
  padding: 0.35em 0.625em 0.75em;
}

/**
 * 1. Correct `color` not being inherited in IE 8/9/10/11.
 * 2. Remove padding so people aren't caught out if they zero out fieldsets.
 */
legend {
  border: 0;
  /* 1 */
  padding: 0;
  /* 2 */
}

/**
 * Remove default vertical scrollbar in IE 8/9/10/11.
 */
textarea {
  overflow: auto;
}

/**
 * Don't inherit the `font-weight` (applied by a rule above).
 * NOTE: the default cannot safely be changed in Chrome and Safari on OS X.
 */
optgroup {
  font-weight: bold;
}

/* Tables
   ========================================================================== */
/**
 * Remove most spacing between table cells.
 */
table {
  border-collapse: collapse;
  border-spacing: 0;
}

td,
th {
  padding: 0;
}

@font-face {
  font-family: 'contax_pro_35_thin_sm_capRg';
  src: url("/fonts/contaxpro_35thinsmcap_macroman/ContaxPro35ThinSC-webfont.eot");
  src: url("/fonts/contaxpro_35thinsmcap_macroman/ContaxPro35ThinSC-webfont.eot?#iefix") format("embedded-opentype"), url("/fonts/contaxpro_35thinsmcap_macroman/ContaxPro35ThinSC-webfont.woff2") format("woff2"), url("/fonts/contaxpro_35thinsmcap_macroman/ContaxPro35ThinSC-webfont.woff") format("woff"), url("/fonts/contaxpro_35thinsmcap_macroman/ContaxPro35ThinSC-webfont.ttf") format("truetype"), url("/fonts/contaxpro_35thinsmcap_macroman/ContaxPro35ThinSC-webfont.svg#contax_pro_35_thin_sm_capRg") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'contax_pro_55regular';
  src: url("/fonts/contaxpro_55regular_macroman/ContaxPro55Rm-webfont.eot");
  src: url("/fonts/contaxpro_55regular_macroman/ContaxPro55Rm-webfont.eot?#iefix") format("embedded-opentype"), url("/fonts/contaxpro_55regular_macroman/ContaxPro55Rm-webfont.woff2") format("woff2"), url("/fonts/contaxpro_55regular_macroman/ContaxPro55Rm-webfont.woff") format("woff"), url("/fonts/contaxpro_55regular_macroman/ContaxPro55Rm-webfont.ttf") format("truetype"), url("/fonts/contaxpro_55regular_macroman/ContaxPro55Rm-webfont.svg#contax_pro_55regular") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'contax_pro_55_sm_capregular';
  src: url("/fonts/contaxpro_55smcap_macroman/ContaxPro55RmSC-webfont.eot");
  src: url("/fonts/contaxpro_55smcap_macroman/ContaxPro55RmSC-webfont.eot?#iefix") format("embedded-opentype"), url("/fonts/contaxpro_55smcap_macroman/ContaxPro55RmSC-webfont.woff2") format("woff2"), url("/fonts/contaxpro_55smcap_macroman/ContaxPro55RmSC-webfont.woff") format("woff"), url("/fonts/contaxpro_55smcap_macroman/ContaxPro55RmSC-webfont.ttf") format("truetype"), url("/fonts/contaxpro_55smcap_macroman/ContaxPro55RmSC-webfont.svg#contax_pro_55_sm_capregular") format("svg");
  font-weight: normal;
  font-style: normal;
}
@font-face {
  font-family: 'contax_pro_75_bold_sm_capbold';
  src: url("/fonts/contaxpro_75boldsmcap_macroman/ContaxPro75BoldSC-webfont.eot");
  src: url("/fonts/contaxpro_75boldsmcap_macroman/ContaxPro75BoldSC-webfont.eot?#iefix") format("embedded-opentype"), url("/fonts/contaxpro_75boldsmcap_macroman/ContaxPro75BoldSC-webfont.woff2") format("woff2"), url("/fonts/contaxpro_75boldsmcap_macroman/ContaxPro75BoldSC-webfont.woff") format("woff"), url("/fonts/contaxpro_75boldsmcap_macroman/ContaxPro75BoldSC-webfont.ttf") format("truetype"), url("/fonts/contaxpro_75boldsmcap_macroman/ContaxPro75BoldSC-webfont.svg#contax_pro_75_bold_sm_capbold") format("svg");
  font-weight: normal;
  font-style: normal;
}
html {
  font-size: 16px;
  -webkit-tap-highlight-color: transparent;
  -moz-osx-font-smoothing: grayscale;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.004);
  text-rendering: optimizeLegibility !important;
  -webkit-font-smoothing: antialiased !important;
  color: #616161;
}

body {
  font-family: contax_pro_55regular, Arial, sans-serif;
  line-height: 1.4;
  font-weight: normal;
  font-style: normal;
}

h1 a, .h1 a, h2 a, .h2 a, h3 a, .h3 a, h4 a, .h4 a, h5 a, .h5 a, h6 a, .h6 a {
  text-decoration: none;
  color: inherit;
}

p {
  font-size: 1.6rem;
  line-height: 2.4rem;
  margin: 0 0 2.4rem 0;
}

h1 {
  font-family: inherit;
  font-weight: 600;
  font-style: inherit;
  font-size: 4.1875rem;
  line-height: 4.5rem;
  margin-top: 1.5rem;
  margin-bottom: 0rem;
}

h2 {
  font-family: inherit;
  font-weight: 600;
  font-style: inherit;
  font-size: 2.5625rem;
  line-height: 3rem;
  margin-top: 1.5rem;
  margin-bottom: 1.5rem;
}

h3 {
  font-family: inherit;
  font-weight: 600;
  font-style: inherit;
  font-size: 1.625rem;
  line-height: 3rem;
  margin-top: 1.5rem;
  margin-bottom: 0rem;
}

h4 {
  font-family: inherit;
  font-weight: 600;
  font-style: inherit;
  font-size: 1rem;
  line-height: 1.5rem;
  margin-top: 1.5rem;
  margin-bottom: 0rem;
}

h5 {
  font-family: inherit;
  font-weight: 400;
  font-style: italic;
  font-size: 1rem;
  line-height: 1.5rem;
  margin-top: 1.5rem;
  margin-bottom: 0rem;
}

 /*
p, ul, ol, pre, table, blockquote {
  margin-top: 0rem;
  margin-bottom: 1.5rem;
}
ul ul, ol ol, ul ol, ol ul {
  margin-top: 0;
  margin-bottom: 0;
}
hr {
  border: 1px solid;
  margin: -1px 0;
}
b, strong, em, small, code {
  line-height: 1;
}
sup {
  line-height: 0;
  position: relative;
  vertical-align: baseline;
  top: -0.5em;
}
sub {
  bottom: -0.25em;
}
a {
  color: #268ED9;
}
*/
.lazyload {
  opacity: 0;
}

.lazyloaded {
  opacity: 1;
}

.card {
  border-radius: 2px;
  box-shadow: -2px 1.5px 4px rgba(0, 0, 0, 0.24), -2px 1.5px 6px rgba(0, 0, 0, 0.12);
}

.mod-slide-item {
  margin: 0 8px;
  background-color: #BDBDBD;
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .mod-slide-item {
    margin: 0 16px;
  }
}
.mod-slide-item.large {
  display: none;
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .mod-slide-item.small {
    display: none;
  }
  .mod-slide-item.large {
    display: inline;
  }
}

.mod-slide-nav-item {
  margin: 0 8px;
  height: 60px;
  background-color: #BDBDBD;
}

.mod-slide-nav-item.is-nav-selected, .mod-slide-nav-item.is-selected {
  opacity: 0.65;
}
@media (min-width: 1284px) {
  .mod-slide-nav-item.is-nav-selected:hover, .mod-slide-nav-item.is-selected:hover {
    opacity: 1;
  }
}

.mod-slide-item-single {
  margin: 0 auto;
  display: block;
  max-width: 96%;
  height: auto;
  background-color: #BDBDBD;
}

.comp-debug {
  position: fixed;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 2400;
}
.comp-debug .grid {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  width: 25%;
  float: left;
  padding-left: 1.92308%;
  padding-right: 1.92308%;
  border-right: 0.1rem solid red;
}
@media (min-width: 544px) {
  .comp-debug .grid {
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    width: 16.66667%;
    float: left;
    padding-left: 1.28205%;
    padding-right: 1.28205%;
  }
}
@media (min-width: 992px) {
  .comp-debug .grid {
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    width: 7.14286%;
    float: left;
    padding-left: 0.54945%;
    padding-right: 0.54945%;
  }
}
.comp-debug .grid .column {
  background-color: red;
  opacity: 0.25;
  height: 2000px;
}
.comp-debug .grid:last-child {
  border-right: none;
}
.comp-debug .row {
  opacity: 0.25;
  border-bottom: 1px solid red;
  padding: 12px 0;
  margin-top: -1px;
}
.comp-debug .row .module {
  background-color: blue;
  opacity: 0.15;
  height: 60px;
}
.comp-debug .baseline {
  opacity: 0.25;
  height: 12px;
  border-bottom: 1px dotted gray;
}

.comp-header {
  padding: 12px 0;
}
.comp-header h1 {
  text-align: center;
  font-weight: normal;
  margin: 0;
  padding: 52px 0;
  font-family: contax_pro_35_thin_sm_capRg, Arial, sans-serif;
  font-size: 40px;
  line-height: 1;
}
@media (min-width: 768px) {
  .comp-header h1 {
    padding: 40px 0;
    font-size: 64px;
  }
}
@media (min-width: 992px) {
  .comp-header h1 {
    padding: 56px 0 24px 0;
  }
}

.comp-footer {
  text-align: center;
  font-size: 12px;
  line-height: 24px;
  padding: 12px 16px;
  margin-top: 80px;
}
@media (min-width: 544px) {
  .comp-footer {
    margin-top: 0;
    position: absolute;
    bottom: 0;
    width: 100%;
  }
}
.comp-footer .long {
  display: none;
}
@media (min-width: 992px) {
  .comp-footer .long {
    display: inline;
  }
}
@media (min-width: 992px) {
  .comp-footer .short {
    display: none;
  }
}

@media (min-width: 544px) {
  #inner-wrap {
    padding-bottom: 64px;
  }
}
@media (min-width: 768px) {
  #inner-wrap {
    padding-bottom: 96px;
  }
}

.comp-navigation {
  text-align: center;
  padding: 12px 0;
  height: 84px;
}
.comp-navigation ul {
  display: inline-block;
  margin: 0;
  padding: 0;
}
.comp-navigation ul li {
  float: left;
  padding: 0 16px;
  list-style: none;
}
.comp-navigation ul li a {
  text-decoration: none;
  color: inherit;
  line-height: 60px;
  font-family: contax_pro_55_sm_capregular, "Helvetica Neue",Helvetica,Arial,sans-serif;
}
.comp-navigation .language-switch a {
  opacity: 0.65;
}
.comp-navigation .language-switch a.active {
  opacity: 1;
}

.close-btn {
  display: block;
  padding: 0;
  border: 0;
  outline: none;
  overflow: hidden;
  white-space: nowrap;
  opacity: 1;
  -webkit-tap-highlight-color: transparent;
}

.nav-btn {
  display: block;
  padding: 0;
  border: 0;
  outline: none;
  overflow: hidden;
  white-space: nowrap;
  -webkit-tap-highlight-color: transparent;
  opacity: 0.5;
}

.nav-btn {
  position: absolute;
  top: 0;
  left: 0;
  padding: 10px;
  margin: 10px;
}

#outer-wrap, #inner-wrap {
  position: absolute;
  width: 100%;
  min-height: 100%;
}

#nav {
  z-index: 200;
  position: relative;
  overflow: hidden;
  width: 100%;
}

#nav .close-btn {
  display: none;
}

@media (min-width: 992px) {
  .nav-btn {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }

  #nav .close-btn {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }
}
@media (max-width: 992px) {
  .comp-navigation ul li {
    text-align: left;
    width: 100%;
  }

  #nav {
    position: absolute;
    top: 0;
    padding-top: 5.25em;
  }

  #nav:not(:target) {
    z-index: 1;
    height: 0;
  }

  #nav:target .close-btn {
    display: block;
  }

  #nav .close-btn {
    position: absolute;
    top: 20px;
    right: 20px;
  }

  .js-ready #nav {
    height: 100%;
    width: 200px;
    background: #757575;
    color: #FFFFFF;
    box-shadow: inset -12px 0 12px -7.5px rgba(0, 0, 0, 0.25);
  }

  .js-ready #nav .close-btn {
    display: block;
  }

  .js-ready #nav {
    left: -200px;
  }

  .js-ready #inner-wrap {
    left: 0;
  }

  .js-nav #inner-wrap {
    left: 200px;
  }

  .csstransforms3d.csstransitions.js-ready #nav {
    left: 0;
    transform: translate3d(-100%, 0, 0);
    backface-visibility: hidden;
  }

  .csstransforms3d.csstransitions.js-ready #inner-wrap {
    left: 0 !important;
    transform: translate3d(0, 0, 0);
    transition: transform 500ms ease;
    backface-visibility: hidden;
  }

  .csstransforms3d.csstransitions.js-nav #inner-wrap {
    transform: translate3d(200px, 0, 0) scale3d(1, 1, 1);
  }
}
#imagelightbox {
  cursor: pointer;
  position: fixed;
  z-index: 10000;
  -ms-touch-action: none;
  touch-action: none;
  box-shadow: -2px 15px 24px rgba(0, 0, 0, 0.22), -2px 19px 76px rgba(0, 0, 0, 0.3);
}

.lightbox-anchor {
  width: 40px;
  height: 40px;
  position: absolute;
  right: 8px;
  text-align: center;
  line-height: 40px;
  font-size: 18px;
  color: #424242;
  opacity: 0.65;
  display: none;
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .lightbox-anchor {
    right: 16px;
  }
}

.carousel-cell.is-selected:focus .lightbox-anchor, .carousel-cell.is-selected:active .lightbox-anchor, .carousel-cell.is-selected:hover .lightbox-anchor {
  display: block;
}

#imagelightbox-loading,
#imagelightbox-loading div {
  border-radius: 50%;
}

#imagelightbox-loading {
  width: 2.5em;
  /* 40 */
  height: 2.5em;
  /* 40 */
  background-color: #444;
  background-color: rgba(0, 0, 0, 0.5);
  position: fixed;
  z-index: 10003;
  top: 50%;
  left: 50%;
  padding: 0.625em;
  /* 10 */
  margin: -1.25em 0 0 -1.25em;
  /* 20 */
  box-shadow: -2px 15px 24px rgba(0, 0, 0, 0.22), -2px 19px 76px rgba(0, 0, 0, 0.3);
}

#imagelightbox-loading div {
  width: 1.25em;
  /* 20 */
  height: 1.25em;
  /* 20 */
  background-color: #fff;
  -webkit-animation: imagelightbox-loading .3s ease infinite;
  animation: imagelightbox-loading .3s ease infinite;
}

@-webkit-keyframes imagelightbox-loading {
  from {
    opacity: .5;
    -webkit-transform: scale(0.75);
  }
  50% {
    opacity: 1;
    -webkit-transform: scale(1);
  }
  to {
    opacity: .5;
    -webkit-transform: scale(0.75);
  }
}
@keyframes imagelightbox-loading {
  from {
    opacity: .5;
    transform: scale(0.75);
  }
  50% {
    opacity: 1;
    transform: scale(1);
  }
  to {
    opacity: .5;
    transform: scale(0.75);
  }
}
/* OVERLAY */
#imagelightbox-overlay {
  background-color: #424242;
  background-color: rgba(66, 66, 66, 0.95);
  position: fixed;
  z-index: 9998;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
}

/* "CLOSE" BUTTON */
#imagelightbox-close {
  width: 40px;
  /* 40 */
  height: 40px;
  /* 40 */
  text-align: left;
  background-color: transparent;
  border: none;
  position: fixed;
  z-index: 10002;
  top: 20px;
  /* 40 */
  right: 20px;
  /* 40 */
  opacity: 0.5;
  -webkit-transition: color .2s ease;
  transition: color .2s ease;
}

#imagelightbox-close:hover,
#imagelightbox-close:focus {
  /*background-color: #111;*/
}

#imagelightbox-close:before,
#imagelightbox-close:after {
  width: 2px;
  background-color: #fff;
  content: '';
  position: absolute;
  top: 20%;
  bottom: 20%;
  left: 50%;
  margin-left: -1px;
}

#imagelightbox-close:before {
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}

#imagelightbox-close:after {
  -webkit-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
}

/* CAPTION */
#imagelightbox-caption {
  text-align: center;
  color: #fff;
  background-color: #666;
  position: fixed;
  z-index: 10001;
  left: 0;
  right: 0;
  bottom: 0;
  padding: 0.625em;
  /* 10 */
}

/* NAVIGATION */
#imagelightbox-nav {
  background-color: #444;
  background-color: rgba(0, 0, 0, 0.5);
  border-radius: 20px;
  position: fixed;
  z-index: 10001;
  left: 50%;
  bottom: 3.75em;
  /* 60 */
  padding: 0.313em;
  /* 5 */
  -webkit-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  transform: translateX(-50%);
}

#imagelightbox-nav button {
  width: 1em;
  /* 20 */
  height: 1em;
  /* 20 */
  background-color: transparent;
  border: 1px solid #fff;
  border-radius: 50%;
  display: inline-block;
  margin: 0 0.313em;
  /* 5 */
}

#imagelightbox-nav button.active {
  background-color: #fff;
}

/* ARROWS */
.imagelightbox-arrow {
  width: 3.75em;
  /* 60 */
  height: 7.5em;
  /* 120 */
  background-color: #444;
  background-color: rgba(0, 0, 0, 0.5);
  vertical-align: middle;
  display: none;
  position: fixed;
  z-index: 10001;
  top: 50%;
  margin-top: -3.75em;
  /* 60 */
}

.imagelightbox-arrow:hover,
.imagelightbox-arrow:focus {
  background-color: rgba(0, 0, 0, 0.75);
}

.imagelightbox-arrow:active {
  background-color: #111;
}

.imagelightbox-arrow-left {
  left: 2.5em;
  /* 40 */
}

.imagelightbox-arrow-right {
  right: 2.5em;
  /* 40 */
}

.imagelightbox-arrow:before {
  width: 0;
  height: 0;
  border: 1em solid transparent;
  content: '';
  display: inline-block;
  margin-bottom: -0.125em;
  /* 2 */
}

.imagelightbox-arrow-left:before {
  border-left: none;
  border-right-color: #fff;
  margin-left: -0.313em;
  /* 5 */
}

.imagelightbox-arrow-right:before {
  border-right: none;
  border-left-color: #fff;
  margin-right: -0.313em;
  /* 5 */
}

#imagelightbox-loading,
#imagelightbox-overlay,
#imagelightbox-close,
#imagelightbox-caption,
#imagelightbox-nav,
.imagelightbox-arrow {
  -webkit-animation: fade-in .2s linear;
  animation: fade-in .2s linear;
}

@-webkit-keyframes fade-in {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@keyframes fade-in {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
}
@media only screen and (max-width: 41.25em) {
  #container {
    width: 100%;
  }

  #imagelightbox-close {
    top: 1.25em;
    /* 20 */
    right: 1.25em;
    /* 20 */
  }

  #imagelightbox-nav {
    bottom: 1.25em;
    /* 20 */
  }

  .imagelightbox-arrow {
    width: 2.5em;
    /* 40 */
    height: 3.75em;
    /* 60 */
    margin-top: -2.75em;
    /* 30 */
  }

  .imagelightbox-arrow-left {
    left: 1.25em;
    /* 20 */
  }

  .imagelightbox-arrow-right {
    right: 1.25em;
    /* 20 */
  }
}
@media only screen and (max-width: 20em) {
  .imagelightbox-arrow-left {
    left: 0;
  }

  .imagelightbox-arrow-right {
    right: 0;
  }
}
.comp-slider {
  height: 336px;
  padding: 12px 0;
  overflow: hidden;
  	/*! Flickity v1.2.1
  http://flickity.metafizzy.co
  ---------------------------------------------- */
  /* draggable */
  /* ---- previous/next buttons ---- */
  /* right to left */
  /* color & size if no SVG - IE8 and Android 2.3 */
  /* ---- page dots ---- */
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .comp-slider {
    height: 592px;
  }
}
.comp-slider .flickity-enabled {
  position: relative;
}
.comp-slider .flickity-enabled:focus {
  outline: none;
}
.comp-slider .flickity-viewport {
  /* overflow: hidden; */
  position: relative;
  height: 100%;
}
.comp-slider .flickity-slider {
  position: absolute;
  width: 100%;
  height: 100%;
}
.comp-slider .flickity-enabled.is-draggable {
  -webkit-tap-highlight-color: transparent;
  tap-highlight-color: transparent;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.comp-slider .flickity-enabled.is-draggable .flickity-viewport {
  cursor: move;
  cursor: -webkit-grab;
  cursor: grab;
}
.comp-slider .flickity-enabled.is-draggable .flickity-viewport.is-pointer-down {
  cursor: -webkit-grabbing;
  cursor: grabbing;
}
.comp-slider .flickity-prev-next-button {
  position: absolute;
  top: 50%;
  width: 44px;
  height: 44px;
  border: none;
  background: white;
  background: rgba(255, 255, 255, 0);
  cursor: pointer;
  /* vertically center */
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.comp-slider .flickity-prev-next-button:hover {
  background: white;
}
.comp-slider .flickity-prev-next-button:focus {
  outline: none;
  box-shadow: 0 0 0 5px #09F;
}
.comp-slider .flickity-prev-next-button:active {
  filter: alpha(opacity=60);
  /* IE8 */
  opacity: 0.6;
}
.comp-slider .flickity-prev-next-button.previous {
  left: 0;
}
.comp-slider .flickity-prev-next-button.next {
  right: 0;
}
.comp-slider .flickity-rtl .flickity-prev-next-button.previous {
  left: auto;
  right: 0;
}
.comp-slider .flickity-rtl .flickity-prev-next-button.next {
  right: auto;
  left: 0;
}
.comp-slider .flickity-prev-next-button:disabled {
  filter: alpha(opacity=30);
  /* IE8 */
  opacity: 0.3;
  cursor: auto;
}
.comp-slider .flickity-prev-next-button svg {
  position: absolute;
  left: 20%;
  top: 20%;
  width: 60%;
  height: 60%;
}
.comp-slider .flickity-prev-next-button .arrow {
  fill: #333;
}
.comp-slider .flickity-prev-next-button.no-svg {
  color: #333;
  font-size: 26px;
}
.comp-slider .flickity-page-dots {
  position: absolute;
  width: 100%;
  bottom: -25px;
  padding: 0;
  margin: 0;
  list-style: none;
  text-align: center;
  line-height: 1;
}
.comp-slider .flickity-rtl .flickity-page-dots {
  direction: rtl;
}
.comp-slider .flickity-page-dots .dot {
  display: inline-block;
  width: 10px;
  height: 10px;
  margin: 0 8px;
  background: #333;
  border-radius: 50%;
  filter: alpha(opacity=25);
  /* IE8 */
  opacity: 0.25;
  cursor: pointer;
}
.comp-slider .flickity-page-dots .dot.is-selected {
  filter: alpha(opacity=100);
  /* IE8 */
  opacity: 1;
}

.js-flickity {
  opacity: 0;
}
.js-flickity .flickity-prev-next-button {
  display: none;
}
.js-flickity.flickity-enabled {
  opacity: 1;
}
.js-flickity.flickity-enabled:hover .flickity-prev-next-button {
  display: block;
}

.gallery-main .flickity-viewport {
  margin-bottom: 24px;
}

/*# sourceMappingURL=slider.php.map */
