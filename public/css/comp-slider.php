html {
  height: 100%;
  min-height: 100%;
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

body {
  position: relative;
  min-height: 100%;
  margin: 0;
}

.clear:before, .clear:after {
  content: " ";
  display: table;
}
.clear:after {
  clear: both;
}

.mod-slide-item {
  margin: 0 8px;
  background-color: #BDBDBD;
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .mod-slide-item {
    margin: 0 16px;
  }
}
.mod-slide-item.large {
  display: none;
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .mod-slide-item.small {
    display: none;
  }
  .mod-slide-item.large {
    display: inline;
  }
}

.mod-slide-nav-item {
  margin: 0 8px;
  height: 60px;
  background-color: #BDBDBD;
}

.mod-slide-nav-item.is-nav-selected, .mod-slide-nav-item.is-selected {
  opacity: 0.65;
}
@media (min-width: 1284px) {
  .mod-slide-nav-item.is-nav-selected:hover, .mod-slide-nav-item.is-selected:hover {
    opacity: 1;
  }
}

.mod-slide-item-single {
  margin: 0 auto;
  display: block;
  max-width: 96%;
  height: auto;
  background-color: #BDBDBD;
}

.comp-slider {
  height: 336px;
  padding: 12px 0;
  overflow: hidden;
  	/*! Flickity v1.2.1
  http://flickity.metafizzy.co
  ---------------------------------------------- */
  /* draggable */
  /* ---- previous/next buttons ---- */
  /* right to left */
  /* color & size if no SVG - IE8 and Android 2.3 */
  /* ---- page dots ---- */
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .comp-slider {
    height: 592px;
  }
}
.comp-slider .flickity-enabled {
  position: relative;
}
.comp-slider .flickity-enabled:focus {
  outline: none;
}
.comp-slider .flickity-viewport {
  /* overflow: hidden; */
  position: relative;
  height: 100%;
}
.comp-slider .flickity-slider {
  position: absolute;
  width: 100%;
  height: 100%;
}
.comp-slider .flickity-enabled.is-draggable {
  -webkit-tap-highlight-color: transparent;
  tap-highlight-color: transparent;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
.comp-slider .flickity-enabled.is-draggable .flickity-viewport {
  cursor: move;
  cursor: -webkit-grab;
  cursor: grab;
}
.comp-slider .flickity-enabled.is-draggable .flickity-viewport.is-pointer-down {
  cursor: -webkit-grabbing;
  cursor: grabbing;
}
.comp-slider .flickity-prev-next-button {
  position: absolute;
  top: 50%;
  width: 44px;
  height: 44px;
  border: none;
  background: white;
  background: rgba(255, 255, 255, 0);
  cursor: pointer;
  /* vertically center */
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
.comp-slider .flickity-prev-next-button:hover {
  background: white;
}
.comp-slider .flickity-prev-next-button:focus {
  outline: none;
  box-shadow: 0 0 0 5px #09F;
}
.comp-slider .flickity-prev-next-button:active {
  filter: alpha(opacity=60);
  /* IE8 */
  opacity: 0.6;
}
.comp-slider .flickity-prev-next-button.previous {
  left: 0;
}
.comp-slider .flickity-prev-next-button.next {
  right: 0;
}
.comp-slider .flickity-rtl .flickity-prev-next-button.previous {
  left: auto;
  right: 0;
}
.comp-slider .flickity-rtl .flickity-prev-next-button.next {
  right: auto;
  left: 0;
}
.comp-slider .flickity-prev-next-button:disabled {
  filter: alpha(opacity=30);
  /* IE8 */
  opacity: 0.3;
  cursor: auto;
}
.comp-slider .flickity-prev-next-button svg {
  position: absolute;
  left: 20%;
  top: 20%;
  width: 60%;
  height: 60%;
}
.comp-slider .flickity-prev-next-button .arrow {
  fill: #333;
}
.comp-slider .flickity-prev-next-button.no-svg {
  color: #333;
  font-size: 26px;
}
.comp-slider .flickity-page-dots {
  position: absolute;
  width: 100%;
  bottom: -25px;
  padding: 0;
  margin: 0;
  list-style: none;
  text-align: center;
  line-height: 1;
}
.comp-slider .flickity-rtl .flickity-page-dots {
  direction: rtl;
}
.comp-slider .flickity-page-dots .dot {
  display: inline-block;
  width: 10px;
  height: 10px;
  margin: 0 8px;
  background: #333;
  border-radius: 50%;
  filter: alpha(opacity=25);
  /* IE8 */
  opacity: 0.25;
  cursor: pointer;
}
.comp-slider .flickity-page-dots .dot.is-selected {
  filter: alpha(opacity=100);
  /* IE8 */
  opacity: 1;
}

.js-flickity {
  opacity: 0;
}
.js-flickity .flickity-prev-next-button {
  display: none;
}
.js-flickity.flickity-enabled {
  opacity: 1;
}
.js-flickity.flickity-enabled:hover .flickity-prev-next-button {
  display: block;
}

.gallery-main .flickity-viewport {
  margin-bottom: 24px;
}

/*# sourceMappingURL=comp-slider.php.map */
