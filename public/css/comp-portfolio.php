html {
  height: 100%;
  min-height: 100%;
  box-sizing: border-box;
}

*, *:before, *:after {
  box-sizing: inherit;
}

body {
  position: relative;
  min-height: 100%;
  margin: 0;
}

.clear:before, .clear:after {
  content: " ";
  display: table;
}
.clear:after {
  clear: both;
}

.mod-portfolio-item {
  background-color: #BDBDBD;
  display: inline-block;
  margin-top: 4px;
  margin-bottom: 4px;
}
.mod-portfolio-item.large {
  display: none;
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .mod-portfolio-item.small {
    display: none;
  }
  .mod-portfolio-item.large {
    display: inline;
  }
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .mod-portfolio-item {
    margin-top: 12px;
    margin-bottom: 12px;
  }
  .mod-portfolio-item:hover {
    transition: all 150ms cubic-bezier(0.86, 0, 0.07, 1);
    margin-top: 0;
    margin-bottom: 24px;
    box-shadow: -2px 15px 24px rgba(0, 0, 0, 0.22), -2px 19px 76px rgba(0, 0, 0, 0.3);
  }
}

.comp-portfolio {
  text-align: center;
}
.comp-portfolio .container {
  font-size: 1px;
}
.comp-portfolio .container .col {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  width: 100%;
  float: left;
  padding-left: 1.92308%;
  padding-right: 1.92308%;
}
@media (min-width: 544px) {
  .comp-portfolio .container .col {
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    float: left;
    padding-left: 1.28205%;
    padding-right: 1.28205%;
  }
}
@media (min-width: 992px) {
  .comp-portfolio .container .col {
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    width: 100%;
    float: left;
    padding-left: 0.54945%;
    padding-right: 0.54945%;
  }
}
.comp-portfolio a {
  padding: 0 4px;
}
@media screen and (min-width: 768px) and (min-height: 600px) {
  .comp-portfolio a {
    padding: 0 8px;
  }
}

/*# sourceMappingURL=comp-portfolio.php.map */
