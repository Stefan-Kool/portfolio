<!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<header class="comp-header">
    <h1><a href="/amp/<?php echo $language; ?>/" >STEFAN KOOL</a></h1>
    <a class="nav-btn" id="nav-open-btn">Menu</a>
</header>
<?php 
    include 'html_amp/components/navigation.php';
?>