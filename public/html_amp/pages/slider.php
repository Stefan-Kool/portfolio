<?php
  $css = "css/slider-min.php";
  include 'head_amp.php';
  include 'html_amp/components/header.php';
  // echo $id.'<br />';

  $galleries_json_url = "data/galleries.json";
  $galleries_json = file_get_contents($galleries_json_url);
  $galleries_json = str_replace('},]',"}]",$galleries_json);
  $portfolio = json_decode($galleries_json, true);

  $images_json_url = "data/images.json";
  $images_json = file_get_contents($images_json_url);
  $images_json = str_replace('},]',"}]",$images_json);
  $images = json_decode($images_json, true);
?>
<!-- Add your site or application content here -->
<main>
    <?php
    $count = 0;
    foreach ($portfolio[$id]['images'] as $keyGallery => $gallery) { $count++; }
    if($count > 1) { ?>


        <amp-carousel height="800"
                      layout="fixed-height"
                      type="carousel">

          <?php foreach ($portfolio[$id]['images'] as $keyGallery => $gallery) {
              $images[$gallery]['src'] ?>
              <amp-img src="/img/h480/erasmus-1.jpg" alt="Welcome" height="400" width="800"></amp-img>
          <?php } ?>
        </amp-carousel>
    <?php } else { ?>
    <div class="comp">
        <div class="container">
            <?php foreach ($portfolio[$id]['images'] as $keyGallery => $gallery) {
                $images[$gallery]['src'] ?>
                <amp-img src="/img/h480/erasmus-1.jpg" alt="Welcome" height="400" width="800"></amp-img>
            <?php }
            } ?>
        </div>
    </div>
</main>
<?php
    $js = "/js/slider-min.js";
    include 'html_amp/components/footer.php';
    include 'foot_amp.php';
?>
