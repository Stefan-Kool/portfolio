<?php
    $css = "css/portfolio-min.php";
    include 'head_amp.php';
    include 'html_amp/components/header.php';

    $galleries_json_url = "data/galleries.json";
    $galleries_json = file_get_contents($galleries_json_url);
    $galleries_json = str_replace('},]',"}]",$galleries_json);
    $portfolio = json_decode($galleries_json, true);

    $images_json_url = "data/images.json";
    $images_json = file_get_contents($images_json_url);
    $images_json = str_replace('},]',"}]",$images_json);
    $images = json_decode($images_json, true);

?>
    <!-- Add your site or application content here -->
    <main>
        <div class="comp-portfolio">
            <div class="container" >
                <div class="col">
                    <?php
                        foreach ($portfolio as $keyGallery => $gallery) {
                            foreach ($gallery['featured'] as $keyImage => $featured) {
                                if($language === 'nl') {
                                    $class = 'color';
                                    $image = $images[$featured]['src'];
                                    $link = '/amp/' . $language . '/' . 'galerij' . '/' . $keyGallery . '/' . $gallery['language'][0]['url'] . '/';
                                    $title = $images[$featured]['language'][0]['title'];
                                    include 'html_amp/modules/portfolio_item.php';
                                }
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
   </main>
<?php
    include 'html_amp/components/footer.php';
    include 'foot_amp.php';
?>