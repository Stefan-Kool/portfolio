<?php
    list($width, $height) = getimagesize('img/h76/'.$image.'');
    $width1 = $width;
 	$height1 = $height;
    list($width, $height) = getimagesize('img/h144/'.$image.'');
    $width2 = $width;
 	$height2 = $height;
    list($width, $height) = getimagesize('img/h152/'.$image.'');
    $width3 = $width;
 	$height3 = $height;
    list($width, $height) = getimagesize('img/h288/'.$image.'');
    $width4 = $width;
 	$height4 = $height;
?>
<a href="<?php echo $link; ?>" >
	<img 	class="mod-portfolio-item lazyload card small"
			src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
			data-srcset="/img/h76/<?php echo $image; ?> 1x,
						/img/h152/<?php echo $image; ?> 2x"
						height="<?php echo $height1; ?>" 
						width="<?php echo $width1; ?>"
						alt="<?php echo $title; ?>" />
	<img 	class="mod-portfolio-item lazyload card large"
			src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
			data-srcset="/img/h144/<?php echo $image; ?> 1x,
						/img/h288/<?php echo $image; ?> 2x"
						height="<?php echo $height2; ?>" 
						width="<?php echo $width2; ?>"
						alt="<?php echo $title; ?>" />
</a>