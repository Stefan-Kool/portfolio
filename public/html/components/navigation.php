<?php 
	$pages_json_url = "data/pages.json";
    $pages_json = file_get_contents($pages_json_url);
    $pages_json = str_replace('},]',"}]",$pages_json);
    $pages = json_decode($pages_json, true);
?>

<nav id="nav" class="comp-navigation" role="navigation" >
    <ul>
 		<?php
            foreach ($pages as $keyPages => $navigationItems) {
				$url_nl = (strlen($navigationItems['language'][0]['url']) > 0) ? '/nl/'.$navigationItems['language'][0]['url'].'/' : '/nl/';
				$url_en = (strlen($navigationItems['language'][0]['url']) > 0) ? '/en/'.$navigationItems['language'][1]['url'].'/' : '/en/';

            	if ($language === 'nl') {
            		echo '<li><a href="'.$url_nl.'">'.$navigationItems['language'][0]['title'].'</a></li>';
            	}
            	if ($language === 'en') {
            		echo '<li><a href="'.$url_en.'">'.$navigationItems['language'][1]['title'].'</a></li>';
            	}
            }
        ?>
        <li class="language-switch">
        	<a href="/nl/<?php echo $currentPage['language'][0]['url'].((strlen($currentPage['language'][0]['url']) == 0) ? '': '/'); ?>" <?php if($language === 'nl') { echo 'class="active"'; } ?> >nl</a>
        	<span> | </span>
        	<a href="/en/<?php echo $currentPage['language'][1]['url'].((strlen($currentPage['language'][1]['url']) == 0) ? '': '/'); ?>" <?php if($language === 'en') { echo 'class="active"'; } ?> >en</a>
    	</li>
        <li>
            <a href="/amp/nl/<?php echo $currentPage['language'][0]['url'].((strlen($currentPage['language'][0]['url']) == 0) ? '': '/'); ?>">AMP</a>
        </li>
    </ul>

    <a class="close-btn" id="nav-close-btn">Close</a>
</nav>