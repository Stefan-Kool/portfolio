<?php function auto_copyright($year = 'auto'){ ?>
   <?php if(intval($year) == 'auto'){ $year = date('Y'); } ?>
   <?php if(intval($year) == date('Y')){ echo intval($year); } ?>
   <?php if(intval($year) < date('Y')){ echo intval($year) . ' - ' . date('Y'); } ?>
   <?php if(intval($year) > date('Y')){ echo date('Y'); } ?>
<?php } ?>

<footer class="comp-footer">
	<?php if($language === 'nl') { ?>
    	<span class="long">e-mail: mail@stefankool.nl - tel: +31.6.245.480.06  -  adres: Boompjes 482 3011 XZ Rotterdam Nederland  -  KvK-nummer: 55763189</span><a class="short" href="/nl/contact/">Contact</a> -  &copy; Stefan Kool <?php auto_copyright('2010'); ?>
    <?php } else { ?>
    	<span class="long">e-mail: mail@stefankool.nl  -  phone: +31.6.245.480.06  -  address: Boompjes 482 3011 XZ Rotterdam the Netherlands  -  KvK-nummer: 55763189</span><a class="short" href="/en/contact/">Contact</a>  -  &copy; Stefan Kool <?php auto_copyright('2010'); ?>
    <?php } ?>
</footer>