<?php
	$css = "css/main.php";
    include 'head.php';
    include 'html/components/header.php';
?>

    <!-- Add your site or application content here -->
    <main>

    	<div class="comp-content">
    		<div class="container">
	            <div class="mod-mini-portfolio " >

	             
	            </div>
		        <div class="spacer">
		        </div>
		        <div class="mod-content">
		            <div class="container">
		            	<aside></aside>
		            	<div></div>
		            	<article>
			            	<h1>Dit is een H1 titel</h1>
			                <h2>Dit is een H2 subtitel</h2>
			                <p>
			                    Dit is een &quot;intro&quot; paragraaf. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula
			                    eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
			                </p>
			                <p>
			                    &quot;Dit is een &quot;quote&quot; paragraaf. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula
			                    eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.&quot;
			                </p>
			                <p>
			                    Dit is een paragraaf. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
			                    sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
			                    Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
			                    Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. <b>Donec</b>
			                    sodales sagittis magna. Sed <a href="#0.1_">consequat</a>, leo eget bibendum sodales, augue velit cursus nunc, quis
			                    gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed,
			                    nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu <i>iturpis</i> hendrerit fringilla.
			                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi
			                    consectetuer lacinia.
			                </p>
			                <p>
			                    Dit is een paragraaf. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
			                    sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
			                    Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
			                    Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. <b>Donec</b>
			                    sodales sagittis magna. Sed <a href="#0.1_">consequat</a>, leo eget bibendum sodales, augue velit cursus nunc, quis
			                    gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed,
			                    nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu <i>iturpis</i> hendrerit fringilla.
			                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi
			                    consectetuer lacinia.
			                </p>
			                <p>
			                    Dit is een paragraaf. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
			                    sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
			                    Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
			                    Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. <b>Donec</b>
			                    sodales sagittis magna. Sed <a href="#0.1_">consequat</a>, leo eget bibendum sodales, augue velit cursus nunc, quis
			                    gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed,
			                    nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu <i>iturpis</i> hendrerit fringilla.
			                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi
			                    consectetuer lacinia.
			                </p>
			                <h3>Dit is een H3 titel</h3>
			                <h4>Dit is een H4 subtitel</h4>
			                <p>
			                    Dit is een paragraaf. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet
			                    iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut,
			                    mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat
			                    pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet,
			                    nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo
			                    pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor
			                    et, hendrerit quis, nisi.
			                </p>
			                <ul>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                </ul>
			                <p>
			                    Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed
			                    aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent
			                    adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero.
			                    Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id,
			                    imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque
			                    facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit
			                    quis, nisi.
			                </p>
			                <ol>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                    <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
			                </ol>
			                <p>
			                    Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed
			                    aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent
			                    adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero.
			                    Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id,
			                    imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque
			                    facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit
			                    quis, nisi.
			                </p>
			                <h5>Dit is een H5 titel</h5>
			                <h6>Dit is een H6 titel</h6>
			                <p>
			                    P-footnote laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices
			                    semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et
			                    pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac
			                    nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris.
			                </p>
		            	</article>
		            </div>
		        </div>
	        </div>
        </div>

    </main>

<?php
	$js = "/js/main.js";
    include 'html/components/footer.php';
    include 'foot.php';
?>