<?php
	$css = "css/main.php";
    include 'head.php';
    include 'html/components/header.php';
?>

    <!-- Add your site or application content here -->
    <main>

    	<div class="comp-content">
    		<div class="container">
	    		<article>
	    			<br /><br />
	    			Stefan Kool fotografie <br />
	    			adres: Boompjes 482 3011 XZ Rotterdam Nederland <br />
	    			e-mail: mail@stefankool.nl <br />
	    			tel: +31.6.245.480.06 <br />
	    			KvK-nummer: 55763189 <br />
	    		</article>
	        </div>
        </div>

    </main>

<?php
	$js = "/js/main.js";
    include 'html/components/footer.php';
    include 'foot.php';
?>