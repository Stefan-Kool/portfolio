<?php
  $css = "css/slider-min.php";
  include 'head.php';
  include 'html/components/header.php';
  // echo $id.'<br />';

  $galleries_json_url = "data/galleries.json";
  $galleries_json = file_get_contents($galleries_json_url);
  $galleries_json = str_replace('},]',"}]",$galleries_json);
  $portfolio = json_decode($galleries_json, true);

  $images_json_url = "data/images.json";
  $images_json = file_get_contents($images_json_url);
  $images_json = str_replace('},]',"}]",$images_json);
  $images = json_decode($images_json, true);
?>
<!-- Add your site or application content here -->
<main>
    <?php
    $count = 0;
    foreach ($portfolio[$id]['images'] as $keyGallery => $gallery) { $count++; }
    if($count > 1) { ?>
    <div class="comp-slider">
        <div class="gallery-main carousel js-flickity" data-flickity-options='{ "cellAlign": "center", "contain": false, "pageDots": false, "imagesLoaded": true, "lazyLoad": 2 }'>
          <?php foreach ($portfolio[$id]['images'] as $keyGallery => $gallery) {
            list($width, $height) = getimagesize('img/h224/'.$images[$gallery]['src'].'');
            $width1 = $width;
            $height1 = $height;
            list($width, $height) = getimagesize('img/h448/'.$images[$gallery]['src'].'');
            $width2 = $width;
            $height2 = $height;
            list($width, $height) = getimagesize('img/h480/'.$images[$gallery]['src'].'');
            $width3 = $width;
            $height3 = $height;
            list($width, $height) = getimagesize('img/h960/'.$images[$gallery]['src'].'');
            $width4 = $width;
            $height4 = $height;
          ?>
          <div class="carousel-cell ">
            <a class="lightbox-anchor" href="/img/src/<?php echo $images[$gallery]['src']; ?>" data-imagelightbox="s"><span class="fa fa-expand"></span></a>
            <img  class="mod-slide-item card lazyload small"
                  src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-srcset="/img/h224/<?php echo $images[$gallery]['src']; ?> 1x,
                          /img/h448/<?php echo $images[$gallery]['src']; ?> 1.5x"
                          height="<?php echo $height1; ?>"
                          width="<?php echo $width1; ?>"
                          alt="" />
            <img class="mod-slide-item card carousel-cell lazyload large"
                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                  data-srcset="/img/h480/<?php echo $images[$gallery]['src']; ?> 1x,
                          /img/h960/<?php echo $images[$gallery]['src']; ?> 1.5x"
                          height="<?php echo $height3; ?>"
                          width="<?php echo $width3; ?>"
                          alt="" />
          </div>
          <?php } ?>
        </div>
        <div class="gallery gallery-nav js-flickity" data-flickity-options='{ "asNavFor": ".gallery-main", "contain": true, "pageDots": false, "prevNextButtons": false }'>
      <?php foreach ($portfolio[$id]['images'] as $keyGallery => $gallery) { 
        list($width, $height) = getimagesize('img/h60/'.$images[$gallery]['src'].'');
        $width1 = $width;
        $height1 = $height;
        list($width, $height) = getimagesize('img/h120/'.$images[$gallery]['src'].'');
        $width2 = $width;
        $height2 = $height;
      ?>
        <img  class="mod-slide-nav-item card carousel-cell lazyload"
              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
              data-srcset="/img/h60/<?php echo $images[$gallery]['src']; ?> 1x,
                           /img/h120/<?php echo $images[$gallery]['src']; ?> 1.5x"
                            height="<?php echo $height1; ?>"
                            width="<?php echo $width1; ?>"
                            alt="" />
      <?php } ?>
  	</div>
    </div>
    <?php } else { ?>
    <div class="comp">
        <div class="container" >
      <?php foreach ($portfolio[$id]['images'] as $keyGallery => $gallery) { 
        list($width, $height) = getimagesize('img/h480/'.$images[$gallery]['src'].'');
        $width1 = $width;
        $height1 = $height;
        list($width, $height) = getimagesize('img/h570/'.$images[$gallery]['src'].'');
        $width2 = $width;
        $height2 = $height;
        list($width, $height) = getimagesize('img/h960/'.$images[$gallery]['src'].'');
        $width3 = $width;
        $height3 = $height;
        list($width, $height) = getimagesize('img/h1140/'.$images[$gallery]['src'].'');
        $width4 = $width;
        $height4 = $height;
      ?>
        <img  class="mod-slide-item-single card lazyload"
          src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
          data-sizes="auto"
          data-srcset="/img/h480/<?php echo $images[$gallery]['src']; ?> <?php echo $width1; ?>w,
                        /img/h570/<?php echo $images[$gallery]['src']; ?> <?php echo $width2; ?>w,
                        /img/h960/<?php echo $images[$gallery]['src']; ?> <?php echo $width3; ?>w,
                        /img/h1140/<?php echo $images[$gallery]['src']; ?> <?php echo $width4; ?>w"
                        alt="" />
        <?php } ?>
            </div>
        </div>
    <?php } ?>
</main>
<?php
    $js = "/js/slider-min.js";
    include 'html/components/footer.php';
    include 'foot.php';
?>
