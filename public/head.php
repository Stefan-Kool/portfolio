<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

         <!-- Title & Description -->
        <title>Stefan Kool - fotografie</title>         
        <meta name="description" content=" ">

        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="">
        <meta itemprop="description" content=" ">
        <meta itemprop="image" content="">

        <!-- Twitter Card data -->
        <meta name="twitter:card" content=" ">
        <meta name="twitter:site" content=" ">
        <meta name="twitter:title" content="">
        <meta name="twitter:description" content=" ">
        <meta name="twitter:creator" content="@ ">
        <!-- Twitter summary card with large image must be at least 280x150px -->
        <meta name="twitter:image:src" content="">

        <!-- Open Graph data -->
        <meta property="og:title" content=""/>
        <meta property="og:type" content="article"/>
        <meta property="og:url" content="https:// "/>
        <meta property="og:image:type" content="image/jpg"/>
        <meta property="og:image" content=""/>
        <meta property="og:description" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="article:published_time" content=""/>
        <meta property="article:modified_time" content=""/>
        <meta property="article:section" content=""/>
        <meta property="article:tag" content=""/>

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- Search engine access -->
        <?php /* <meta name="robots" content="index, follow"> */ ?>
        <meta name="robots" content="noindex, nofollow" />

        <style>
            <?php include $css; ?>
        </style>
        <script>
            <?php include 'js/vendor/modernizr-custom.php'; ?>
            <?php include 'js/vendor/picturefill.min.php'; ?>
            <?php include 'js/vendor/lazysizes.min.php'; ?>
        </script>
    </head>
    <body>
        <div id="outer-wrap">
            <div id="inner-wrap">